defmodule Buildah.Apk.Sudo do

    alias Buildah.{Apk, Apk.Catatonit, Cmd}

    def on_container_adding_user_user(container, options) do
        user = "user"
        {catatonit_exec_, 0} = Cmd.run(
            container,
            ["sh", "-c", "command -v " <> Catatonit.catatonit_cmd()]
        )
        Apk.packages_no_cache(container, [
            "setpriv",
            "sudo"
        ], options)
        # Apk.packages_no_cache(container, [
            # "--repository", "http://dl-cdn.alpinelinux.org/alpine/edge/main",
            # "runuser"
        # ], options)
        {adduser_cli_result, 0} = Cmd.run(
            container,
            ["adduser", "-D", user]
        )
        IO.puts(adduser_cli_result)
        {uid_, 0} = Cmd.run(
            container,
            ["id", "-u", user]
        )
        {gid_, 0} = Cmd.run(
            container,
            ["id", "-g", user]
        )
        {_, 0} = Cmd.run(
            container,
            ["sh", "-c", "echo 'user ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers.d/user"]
        )
        {_, 0} = Cmd.run(container, ["sh", "-c", "command -v whoami"])
        {_, 0} = Cmd.run(container, ["sh", "-c", "command -v sudo"])
        {_, 0} = Cmd.run(container, ["sh", "-c", "command -v su"])
        {_, 0} = Cmd.run(container, ["su", "-c", "whoami", "-", "user"], into: IO.stream(:stdio, :line))
        {_, 0} = Cmd.run(container, ["sh", "-c", "command -v setpriv"])
        {_, 0} = Cmd.run(container, ["setpriv", "--reuid=#{String.trim(uid_)}", "--regid=#{String.trim(gid_)}", "--init-groups", "--", "whoami"], into: IO.stream(:stdio, :line))
        {_, 0} = Cmd.config(
            container,
            entrypoint: "[
                \"#{String.trim(catatonit_exec_)}\",
                \"--\",
                \"env\",
                \"HOME=/home/user\",
                \"LOGNAME=user\",
                \"USER=user\",
                \"setpriv\",
                \"--reuid=#{String.trim(uid_)}\",
                \"--regid=#{String.trim(gid_)}\",
                \"--init-groups\",
                \"--\"
                ]",
            # entrypoint: "[\"/bin/su\", \"-\", \"user\"]", # Does not work well.
            # entrypoint: "[\"#{String.trim(catatonit_exec_)}\", \"--\", \"/sbin/runuser\", \"--user\", \"user\", \"--\"]", # Works.
            # --reset-env may be needed for setting env variables, but it removes the ones already set.
            env: ["SUDO=sudo"],
            into: IO.stream(:stdio, :line)
        )
    end

    def test(container, image_ID, options) do
        {_, 0} = Cmd.run(container, ["whoami"], into: IO.stream(:stdio, :line))
        {_, 0} = Cmd.run(container, ["sudo", "whoami"], into: IO.stream(:stdio, :line))
        {_, 0} = Cmd.run(container, ["sh", "-c", "command -v printenv"], into: IO.stream(:stdio, :line))
        # Test setpriv --reset-env: HOME, LOGNAME, PATH, SHELL, TERM, USER
        {_, 0} = Podman.Cmd.run(image_ID, ["printenv", "HOME"],
            tty: true, rm: true, into: IO.stream(:stdio, :line)
        )
        {_, 0} = Podman.Cmd.run(image_ID, ["printenv", "LOGNAME"],
            tty: true, rm: true, into: IO.stream(:stdio, :line)
        )
        {_, 0} = Podman.Cmd.run(image_ID, ["printenv", "PATH"],
            tty: true, rm: true, into: IO.stream(:stdio, :line)
        )
        # {_, 0} = Podman.Cmd.run(image_ID, ["printenv", "SHELL"],
            # tty: true, rm: true, into: IO.stream(:stdio, :line)
        # )
        {_, 0} = Podman.Cmd.run(image_ID, ["printenv", "TERM"],
            tty: true, rm: true, into: IO.stream(:stdio, :line)
        )
        {_, 0} = Podman.Cmd.run(image_ID, ["printenv", "USER"],
            tty: true, rm: true, into: IO.stream(:stdio, :line)
        )

        {_, 0} = Podman.Cmd.run(image_ID, ["printenv", "SUDO"],
            tty: true, rm: true, into: IO.stream(:stdio, :line)
        )
        {_, 0} = Podman.Cmd.run(image_ID, ["whoami"],
            tty: true, rm: true, into: IO.stream(:stdio, :line)
        )
        {_, 0} = Podman.Cmd.run(image_ID, ["sudo", "whoami"],
            tty: true, rm: true, into: IO.stream(:stdio, :line)
        )
        {_, 0} = Podman.Cmd.run(image_ID, ["sh", "-c", "$SUDO whoami"],
            tty: true, rm: true, into: IO.stream(:stdio, :line)
        )
        Apk.packages_no_cache(container, ["psmisc"], options)
        {_, 0} = Cmd.run(container, ["pstree"], into: IO.stream(:stdio, :line))
        {_, 0} = Podman.Cmd.run(image_ID, ["pstree"],
            tty: true, rm: true, into: IO.stream(:stdio, :line)
        )
    end

end
